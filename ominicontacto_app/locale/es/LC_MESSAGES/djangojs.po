# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 18:29+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ominicontacto_app/static/ominicontacto/JS/agente/omlAPI.js:40
#: ominicontacto_app/static/ominicontacto/JS/agente/omlAPI.js:56
#: ominicontacto_app/static/ominicontacto/JS/agente/omlAPI.js:72
#: ominicontacto_app/static/ominicontacto/JS/agente/omlAPI.js:94
#: ominicontacto_app/static/ominicontacto/JS/agente/omlAPI.js:118
#: ominicontacto_app/static/ominicontacto/JS/agente/omlAPI.js:142
#: ominicontacto_app/static/ominicontacto/JS/agente/omlAPI.js:163
#: ominicontacto_app/static/ominicontacto/JS/supervision/campanas_entrantes.js:34
#: ominicontacto_app/static/ominicontacto/JS/supervision/campanas_salientes.js:34
msgid "Error al ejecutar => "
msgstr "Error al ejecutar => "

#: ominicontacto_app/static/ominicontacto/JS/agente/omlAPI.js:143
#: ominicontacto_app/static/ominicontacto/JS/agente/omlAPI.js:164
msgid "No se pudo iniciar la llamada. Intente Nuevamente."
msgstr "No se pudo iniciar la llamada. Intente Nuevamente."

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:175
msgid "Seleccione una opción válida"
msgstr "Seleccione una opción válida"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:203
#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:415
#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:614
msgid "Conectado a %(fromUser)s"
msgstr "Conectado a %(fromUser)s"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:232
#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:246
msgid "Conectado"
msgstr "Conectado"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:239
#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsView.js:319
msgid "Desconectado"
msgstr "Desconectado"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:260
msgid "Llamando"
msgstr "Llamando"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:267
msgid "En llamado"
msgstr "En llamado"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:274
#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:281
msgid "Transfiriendo"
msgstr "Transfiriendo"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:288
msgid "Recibiendo llamado"
msgstr "Recibiendo llamado"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:295
msgid "En espera"
msgstr "En espera"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:309
msgid "Registrando.."
msgstr "Registrando.."

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:325
msgid "Agente registrado"
msgstr "Agente registrado"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:328
msgid "Agente no registrado, contacte a su administrador"
msgstr "Agente no registrado, contacte a su administrador"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:335
msgid "Agente en pausa"
msgstr "Agente en pausa"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:338
msgid "No se puede obtener la pausa, contacte a su administrador"
msgstr "No se puede obtener la pausa, contacte a su administrador"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:347
#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:437
msgid "Disponible"
msgstr "Disponible"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:350
msgid "No se puede liberar la pausa, contacte a su administrador"
msgstr "No se puede liberar la pausa, contacte a su administrador"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:487
msgid "Obteniendo pausa: %(pause_name)s"
msgstr "Obteniendo pausa: %(pause_name)s"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:509
msgid "Liberando pausa..."
msgstr "Liberando pausa..."

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:567
msgid "Ocupado, intente maás tarde"
msgstr "Ocupado, intente maás tarde"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:570
msgid "Rechazado, intente maás tarde"
msgstr "Rechazado, intente maás tarde"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:573
msgid "No disponible, contacte a su administrador"
msgstr "No disponible, contacte a su administrador"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:577
msgid "Error, verifique el número marcado"
msgstr "Error, verifique el número marcado"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:581
msgid "Error de autenticación, contacte a su administrador"
msgstr "Error de autenticación, contacte a su administrador"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:586
msgid "Error, Falta SDP"
msgstr "Error, Falta SDP"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:589
msgid "Dirección incompleta"
msgstr "Dirección incompleta"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:593
msgid "Servicio no disponible, contacte a su administrador"
msgstr "Servicio no disponible, contacte a su administrador"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:599
msgid "Error WebRTC: El usuario no permite acceso al medio"
msgstr "Error WebRTC: El usuario no permite acceso al medio"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsController.js:604
msgid "Error: Llamado fallido"
msgstr "Error: Llamado fallido"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsView.js:320
msgid "Registrado"
msgstr "Registrado"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsView.js:321
msgid "No Registrado"
msgstr "No Registrado"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsView.js:323
msgid "El SIP Proxy no responde, contacte a su administrador"
msgstr "El SIP Proxy no responde, contacte a su administrador"

#: ominicontacto_app/static/ominicontacto/JS/agente/phoneJsView.js:327
msgid "Fallo en la registración, contacte a su administrador"
msgstr "Fallo en la registración, contacte a su administrador"

#: ominicontacto_app/static/ominicontacto/JS/campanasPreviewAgente.js:36
msgid "Buscar:"
msgstr "Buscar:"

#: ominicontacto_app/static/ominicontacto/JS/campanasPreviewAgente.js:38
msgid "Primero"
msgstr "Primero"

#: ominicontacto_app/static/ominicontacto/JS/campanasPreviewAgente.js:39
msgid "Anterior"
msgstr "Anterior"

#: ominicontacto_app/static/ominicontacto/JS/campanasPreviewAgente.js:40
msgid "Siguiente"
msgstr "Siguiente"

#: ominicontacto_app/static/ominicontacto/JS/campanasPreviewAgente.js:41
msgid "Último"
msgstr "Último"

#: ominicontacto_app/static/ominicontacto/JS/campanasPreviewAgente.js:43
#: ominicontacto_app/static/ominicontacto/JS/contactosList.js:109
msgid "Mostrar _MENU_ entradas"
msgstr "Mostrar _MENU_ entradas"

#: ominicontacto_app/static/ominicontacto/JS/campanasPreviewAgente.js:44
#: ominicontacto_app/static/ominicontacto/JS/contactosList.js:110
msgid "Mostrando _START_ a _END_ de _TOTAL_ entradas"
msgstr "Mostrando _START_ a _END_ de _TOTAL_ entradas"

#: ominicontacto_app/static/ominicontacto/JS/campanasPreviewAgente.js:79
msgid ""
"OPS, se venció el tiempo de asignación de este contacto.Por favor intente "
"solicitar uno nuevo"
msgstr ""
"OPS, se venció el tiempo de asignación de este contacto.Por favor intente "
"solicitar uno nuevo"

#: ominicontacto_app/static/ominicontacto/JS/campanasPreviewAgente.js:112
msgid ""
"Contacto asignado por llamado previo. Califique el contacto o liberelo para "
"poder recibir un nuevo contacto."
msgstr ""
"Contacto asignado por llamado previo. Califique el contacto o liberelo para "
"poder recibir un nuevo contacto."

#: ominicontacto_app/static/ominicontacto/JS/campanasPreviewAgente.js:157
msgid "Contacto Liberado"
msgstr "Contacto Liberado"

#: ominicontacto_app/static/ominicontacto/JS/campanasPreviewAgente.js:164
msgid "No se pudo liberar al contacto. Intente pedir otro."
msgstr "No se pudo liberar al contacto. Intente pedir otro."

#: ominicontacto_app/static/ominicontacto/JS/contactosList.js:39
msgid "Modificar"
msgstr "Modificar"

#: ominicontacto_app/static/ominicontacto/JS/contactosList.js:52
msgid "Llamar"
msgstr "Llamar"

#: ominicontacto_app/static/ominicontacto/JS/contactosList.js:79
msgid "Enviar email"
msgstr "Enviar email"

#: ominicontacto_app/static/ominicontacto/JS/contactosList.js:101
msgid "Buscar: "
msgstr "Buscar: "

#: ominicontacto_app/static/ominicontacto/JS/contactosList.js:102
msgid "(filtrando de un total de _MAX_ contactos)"
msgstr "(filtrando de un total de _MAX_ contactos)"

#: ominicontacto_app/static/ominicontacto/JS/contactosList.js:104
msgid "Primero "
msgstr "Primero "

#: ominicontacto_app/static/ominicontacto/JS/contactosList.js:105
msgid "Anterior "
msgstr "Anterior "

#: ominicontacto_app/static/ominicontacto/JS/contactosList.js:106
msgid " Siguiente"
msgstr " Siguiente"

#: ominicontacto_app/static/ominicontacto/JS/contactosList.js:107
msgid " Último"
msgstr " Último"

#: ominicontacto_app/static/ominicontacto/JS/formularios.js:40
msgid "Ocultar"
msgstr "Ocultar"

#: ominicontacto_app/static/ominicontacto/JS/formularios.js:47
msgid "Mostrar"
msgstr "Mostrar"

#: ominicontacto_app/static/ominicontacto/JS/l18n_constantes.js:1
msgid "Agregar campo"
msgstr "Agregar campo"

#: ominicontacto_app/static/ominicontacto/JS/l18n_constantes.js:2
msgid "Remover"
msgstr "Remover"

#: ominicontacto_app/static/ominicontacto/JS/l18n_constantes.js:3
msgid "Sin Acción"
msgstr "Sin Acción"

#: ominicontacto_app/static/ominicontacto/JS/l18n_constantes.js:4
msgid "Gestión"
msgstr "Gestión"

#: ominicontacto_app/static/ominicontacto/JS/l18n_constantes.js:5
msgid "Agregar parámetro"
msgstr "Agregar parámetro"

#: ominicontacto_app/static/ominicontacto/JS/l18n_constantes.js:6
msgid "Agregar agente"
msgstr "Agregar agente"

#: ominicontacto_app/static/ominicontacto/JS/l18n_constantes.js:7
msgid "Agregar regla"
msgstr "Agregar regla"

#: ominicontacto_app/static/ominicontacto/JS/l18n_constantes.js:8
msgid "Agregar patron de discado"
msgstr "Agregar patron de discado"

#: ominicontacto_app/static/ominicontacto/JS/l18n_constantes.js:9
msgid "Agregar troncal"
msgstr "Agregar troncal"

#: ominicontacto_app/static/ominicontacto/JS/l18n_constantes.js:10
msgid "Agregar destino"
msgstr "Agregar destino"

#: ominicontacto_app/static/ominicontacto/JS/l18n_constantes.js:11
msgid "Agregar validación"
msgstr "Agregar validación"

#: ominicontacto_app/static/ominicontacto/JS/ranges-datepicker.js:3
msgid "Hoy"
msgstr "Hoy"

#: ominicontacto_app/static/ominicontacto/JS/ranges-datepicker.js:4
msgid "Ayer"
msgstr "Ayer"

#: ominicontacto_app/static/ominicontacto/JS/ranges-datepicker.js:5
msgid "Últimos 7 Días"
msgstr "Últimos 7 Días"

#: ominicontacto_app/static/ominicontacto/JS/ranges-datepicker.js:6
msgid "Últimos 30 Días"
msgstr "Últimos 30 Días"

#: ominicontacto_app/static/ominicontacto/JS/ranges-datepicker.js:7
msgid "Este mes"
msgstr "Este mes"

#: ominicontacto_app/static/ominicontacto/JS/ranges-datepicker.js:8
msgid "Último Mes"
msgstr "Último Mes"

#: ominicontacto_app/static/ominicontacto/JS/ranges-datepicker.js:11
msgid "Desde el inicio"
msgstr "Desde el inicio"

#~ msgid "Llamando: %(dialedNumber)s"
#~ msgstr "Llamando: %(dialedNumber)s"
